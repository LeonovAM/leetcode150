﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using System.Xml.Linq;
using static Interview150.Program;

namespace Interview150
{
    public class Program
    {
        public class TreeNode
        {
            public int _val;
            public TreeNode _left;
            public TreeNode _right;
            public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
            {
                _val = val;
                _left = left;
                _right = right;
            }
        }


        //[JsonDerivedType(typeof(Track))]
        //[JsonDerivedType(typeof(Jeep))]
        public class Vehicle
        {
            public int Power { get; set; }

            public int Wheels { get; set; }
        }

        public class Track : Vehicle
        {
            public bool Trailer { get; set; }
        }

        public class Jeep : Vehicle
        {
            public bool Luxery { get; set; }
        }

        private static int? _prev;
        private static bool _checked = true;

        public class Node
        {
            public int val;
            public IList<Node> neighbors;

            public Node()
            {
                val = 0;
                neighbors = new List<Node>();
            }

            public Node(int _val)
            {
                val = _val;
                neighbors = new List<Node>();
            }

            public Node(int _val, List<Node> _neighbors)
            {
                val = _val;
                neighbors = _neighbors;
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Hello, World!");
            //Console.WriteLine(string.Join("-", TwoSum(new int[17] { 1, 1, 1, 1, 1, 4, 1, 1, 1, 1, 1, 7, 1, 1, 1, 1, 1 }, 11)));
            //Console.WriteLine(string.Join("-", RemoveDuplicates(new int[9] { 0, 0, 1, 1, 1, 1, 2, 3, 3 })));
            //Console.WriteLine(string.Join("-", TwoSum2(new int[4] { 2, 7, 11, 15 }, 9)));
            //Console.WriteLine(string.Join("-", MinSubArrayLen(15, [ 2,3,1,2,4,3,1,4,3,2 ])));
            //010
            //001
            //111
            //000

            //GameOfLife([[0, 1, 0], [0, 0, 1], [1, 1, 1], [0, 0, 0]]);
            //Console.WriteLine(string.Join("-", SummaryRanges([-2147483648, -2147483647, 2147483647])));

            //Console.WriteLine(isHappy(3));
            //Console.WriteLine(string.Join("-", PlusOne([9, 9])));
            //Console.WriteLine(IsAnagram("rat", "cat"));
            //Merge([[1, 4], [0, 0]]);
            //Console.WriteLine(SimplifyPath("/a//b////c/d//././/.."));
            //int a = 5;
            //bool b = Recursive(ref a);

            //Dictionary<int, int> dic = new();
            //dic
            //Solution sol = new();
            //sol.BuildTree([3, 9, 20, 15, 7], [9, 3, 15, 20, 7]);

            //Console.WriteLine(EncodeFile(File.ReadAllLines("c:\\Users\\wonder\\Downloads\\coding_qual_input.txt")));

            /*
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Jeep));

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.OmitXmlDeclaration = true;
            StringWriter memory = new StringWriter();
            XmlWriter xmlWriter = XmlWriter.Create(memory, settings);

            Vehicle track = new Jeep() { Power = 152, Wheels = 8, Luxery = true };

            xmlSerializer.Serialize(xmlWriter, track);
            string data = memory.ToString();

            string data_2 = JsonSerializer.Serialize<object>(track);
            */

            //TreeNode root = new(5, new(4), new(6));
            //Check(root);
            //Console.WriteLine(IsPalindrome("A man, a plan, a canal: Panama"));

            //Console.WriteLine(SearchInsert([1, 2, 3, 8, 12, 13, 14, 15, 20, 25], 9));
            //Console.WriteLine(FindPeakElement([5,6,7]));

            //int bigValue = 1_234_569;
            //Console.WriteLine(bigValue.ToString("#,#.0#"));

            //Console.WriteLine(Search([6, 7, 0, 1, 2, 4, 5], 1));
            //Console.WriteLine(string.Join(", ", SearchRange([2], 2)));
            //Console.WriteLine(SearchMatrix([[1, 3, 5, 7], [10, 11, 16, 20], [23, 30, 34, 60]], 30));
            //Console.WriteLine(SearchMatrix([[1, 3]], 3));

            //Merge([-1, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0], 5, [-1, -1, 0, 0, 1, 2], 6);
            //LetterCombinations
            //("23");

            //Combine(4, 3);
            //Permute([1, 2, 3]);

            //CombinationSum([2, 3, 6, 7], 7);
            //Console.WriteLine(Exist([['A', 'B', 'C', 'E'], ['S', 'F', 'C', 'S'], ['A', 'D', 'E', 'E']], "SEE"));

            //Console.WriteLine(AddBinary("1010", "1011"));

            //Console.WriteLine(reverseBits(1));
            //Console.WriteLine(HammingWeight(128));
            //Console.WriteLine(SingleNumber([2,2,1,5,5]));
            //Console.WriteLine(SingleNumber2([3,3,3,2]));

            //Console.WriteLine(RemoveElement([2], 3));
            //Console.WriteLine(RemoveDuplicates2([0,0,0,1,1,2,2]));

            //Console.WriteLine(MajorityElement([1,1,1,1,1,2,3,4,5]));
            //Console.WriteLine(MaxProfit([7, 1, 5, 3, 6, 4]));

            //GenerateParenthesis(3);
            //Console.WriteLine(CanCompleteCircuit([0,0,0,0,1,0], [0,0,0,0,0,0]));

            //Console.WriteLine(LengthOfLastWord("   fly me   to   the moon  "));
            //Console.WriteLine(ReverseWords("  hello world  "));

            //Console.WriteLine(findKthLargest([3, 2, 3, 1, 2, 4, 5, 5, 6], 4));
            //SortList(head);

            //ListNodeN left = new ListNodeN(1, new ListNodeN(2, new ListNodeN(3, new ListNodeN(4, new ListNodeN(5)))));
            //ListNodeN right = new ListNodeN(0, new ListNodeN(1, new ListNodeN(2)));
            //MergeTwoLists(left, right);

            //Merge2([1,2,0,0], 2, [1,4], 2);
            //RotateRight(right, 4);

            //Tribonacci(3);

            //ReverseBetween(left, 2, 4);
            //LongestIdealString("pvjcci", 4);

            //RomanToInt("MCMXCIV");
            //LongestConsecutive([100, 4, 200, 1, 3, 2]);
            //Console.WriteLine(MinFallingPathSum2([[-2, -18, 31, -10, -71, 82, 47, 56, -14, 42], [-95, 3, 65, -7, 64, 75, -51, 97, -66, -28], [36, 3, -62, 38, 15, 51, -58, -90, -23, -63], [58, -26, -42, -66, 21, 99, -94, -95, -90, 89], [83, -66, -42, -45, 43, 85, 51, -86, 65, -39], [56, 9, 9, 95, -56, -77, -2, 20, 78, 17], [78, -13, -55, 55, -7, 43, -98, -89, 38, 90], [32, 44, -47, 81, -1, -55, -5, 16, -81, 17], [-87, 82, 2, 86, -88, -58, -91, -79, 44, -9], [-96, -14, -52, -8, 12, 38, 84, 77, -51, 52]]));
            //Console.WriteLine(MinFallingPathSum2([[1, 2, 3], [4, 5, 6], [7, 8, 9]]));

            //Console.WriteLine(IsSubsequence("aaaaaa", "bbaaaa"));
            //SortedArrayToBST([1,2,3,4,5]);
            // Rob([1,3,1]);

            //Console.WriteLine(NumIslands([['1', '1', '1', '1', '0'], ['1', '1', '0', '1', '0'], ['1', '1', '0', '0', '0'], ['0', '0', '0', '0', '0']]));

            //Console.WriteLine(NumIslands([['1', '1', '0', '0', '0'], ['1', '1', '0', '0', '0'], ['0', '0', '1', '0', '0'], ['0', '0', '0', '1', '1']]));

            //Solve([['X', 'X', 'X', 'X'], ['X', 'O', 'O', 'X'], ['X', 'X', 'O', 'X'], ['X', 'O', 'X', 'X']]);
            //Solve([['X', 'O', 'X', 'O', 'X', 'O'], ['O', 'X', 'O', 'X', 'O', 'X'], ['X', 'O', 'X', 'O', 'X', 'O'], ['O', 'X', 'O', 'X', 'O', 'X']]);

            //WonderfulSubstrings("faba");
            /*
            Node a_1 = new Node() { val = 1 };
            Node a_2 = new Node() { val = 2 };
            Node a_3 = new Node() { val = 3 };
            Node a_4 = new Node() { val = 4 };

            a_1.neighbors = new List<Node> { a_2, a_4 };
            a_2.neighbors = new List<Node> { a_1, a_3 };

            CloneGraph(a_1);
            */

            //HIndex(/*[3, 0, 6, 1, 5]*/[1,3,1]/*[11, 15]*/);
            /*
            CalcEquation(new List<IList<string>>() 
                { 
                    new List<string>() {"a", "b"}, 
                    new List<string>() {"b", "c"} 
                }, [2, 3],
                new List<IList<string>>()
                { 
                    new List<string>() { "a", "c" }, 
                    new List<string>() { "b", "a" }, 
                    new List<string>() { "a", "e" },
                    new List<string>() { "a", "a" },
                    new List<string>() { "x", "x" }
                });*/
            /*
            CalcEquation(new List<IList<string>>()
                {
                    new List<string>() {"a", "b"},
                    new List<string>() {"e", "f"},
                    new List<string>() {"b", "e"}
                }, [3.4, 1.4, 2.3],
                new List<IList<string>>()
                {
                    new List<string>() { "b", "a" },
                    new List<string>() { "a", "f" },
                    new List<string>() { "f", "f" },
                    new List<string>() { "e", "e" },
                    new List<string>() { "c", "c" },
                    new List<string>() { "a", "c" },
                    new List<string>() { "f", "e" }
                });
            */
            /*
            CalcEquation(new List<IList<string>>()
                {
                    new List<string>() {"x1", "x2"},
                    new List<string>() {"x2", "x3"},
                    new List<string>() {"x1", "x4"},
                    new List<string>() {"x2", "x5"}
                }, [3.0, 0.5, 3.4, 5.6],
                new List<IList<string>>()
                {
                    new List<string>() { "x2", "x4" },
                    new List<string>() { "x1", "x5" },
                    new List<string>() { "x1", "x3" },
                    new List<string>() { "x5", "x5" },
                    new List<string>() { "x5", "x1" },
                    new List<string>() { "x3", "x4" },
                    new List<string>() { "x4", "x3" },
                    new List<string>() { "x6", "x6" },
                    new List<string>() { "x0", "x0" }
                });
            */

            CanFinish(2, [[2, 0], [1, 0], [3, 1], [3, 2], [1, 3]]);

        }

        private static bool DFSCheckFinish(int course, Dictionary<int, HashSet<int>> dic, HashSet<int> visited)
        {
            if (visited.Contains(course))
            {
                return false;
            }

            if (!dic.ContainsKey(course) || dic[course].Count == 0)
            {
                return true;
            }

            visited.Add(course);

            foreach (int item in dic[course])
            {
                if (!DFSCheckFinish(item, dic, visited))
                {
                    return false;
                }
            }

            visited.Remove(course);
            dic[course].Clear();
            return true;
        }

        public static bool CanFinish(int numCourses, int[][] prerequisites)
        {
            //  207. Course Schedule
            Dictionary<int, HashSet<int>> dic = new();

            foreach (int[] item in prerequisites)
            {
                if (!dic.TryGetValue(item[0], out _))
                {
                    dic[item[0]] = new();
                }
                dic[item[0]].Add(item[1]);
            }

            HashSet<int> visited = new();
            for (int course = 0; course < numCourses; course++)
            {
                if (!DFSCheckFinish(course, dic, visited))
                {
                    return false;
                }
            }

            return true;
        }

        private static string Find(Dictionary<string, string> parents, Dictionary<string, double> weights, string parent)
        {
            if (parents[parent] != parent)
            {
                string p = parents[parent];
                parents[parent] = Find(parents, weights, p);
                weights[parent] *= weights[p];
            }
            return parents[parent];
        }

        private static double DFSPath(string from, string to, Dictionary<string, Dictionary<string, double>> graph, List<string> visited)
        {
            if (from == to)
            {
                return 1;
            }

            if (graph[from].ContainsKey(to))
            {
                return graph[from][to];
            }

            double res = -1;

            visited.Add(from);

            foreach (var item in graph[from].Keys)
            {
                if (!visited.Contains(item))
                {
                    res = DFSPath(item, to, graph, visited);

                    if (res != -1)
                    {
                        res *= graph[from][item];
                        break;
                    }
                }
            }

            visited.Remove(from);

            return res;
        }

        public static double[] CalcEquation(IList<IList<string>> equations, double[] values, IList<IList<string>> queries)
        {
            //  399.Evaluate Division
            Dictionary<string, Dictionary<string, double>> graph = new();

            foreach ((string numer, string denomin, double weight) in equations.Zip(values, (left, right) => ( left[0], left[1], right )))
            {
                if (!graph.ContainsKey(numer))
                {
                    graph[numer] = new();
                }
                if (!graph.ContainsKey(denomin))
                {
                    graph[denomin] = new();
                }

                graph[numer][denomin] = weight;
                graph[denomin][numer] = 1 / weight;
            }

            List<double> result = new();
            List<string> visited = new();

            foreach (IList<string> item in queries)
            {
                if (!graph.ContainsKey(item[0]))
                {
                    result.Add(-1);
                    continue;
                }

                if (graph[item[0]].ContainsKey(item[1]))
                {
                    result.Add(graph[item[0]][item[1]]);
                    continue;
                }

                visited.Add(item[0]);
                result.Add(DFSPath(item[0], item[1], graph, visited));
                visited.Clear();
            }
             
            return result.ToArray();
        }

        public static int HIndex(int[] citations)
        {
            //  274. H-Index
            Array.Sort(citations);
            int hindex = 0;
            int right = citations[^1];
            int search = 0;
            while (hindex <= right)
            {
                if (citations.Length - search < hindex)
                {
                    break;
                }

                if (hindex >= citations[search])
                {
                    search++;
                }

                hindex++;
            }

            return hindex - 1;
        }

        private static void DfsGraph(Node node, Dictionary<Node, Node> dp)
        {
            if ((node?.neighbors.Count ?? 0) == 0)
            {
                return;
            }

            foreach (Node n in node.neighbors)
            {
                Node cloneNode = dp[node];
                Node linkedNode;
                if (!dp.TryGetValue(n, out linkedNode))
                {
                    linkedNode = new Node() { val = n.val };
                    dp.Add(n, linkedNode);

                    cloneNode.neighbors.Add(linkedNode);
                    DfsGraph(n, dp);
                }
                else
                {
                    cloneNode.neighbors.Add(linkedNode);
                }
            }
        }

        public static Node CloneGraph(Node node)
        {
            if (node == null)
            {
                return null;
            }

            Dictionary<Node, Node> dp = new();

            Node cloneNode = new() { val =  node.val };
            dp.Add(node, cloneNode);

            DfsGraph(node, dp);

            return cloneNode;
        }

        public static long WonderfulSubstrings(string word)
        {
            long state = 0;
            long totalCount = 0;

            long[] stateCount = new long[1024];
            stateCount[0] = 1;
            foreach (char ch in word)
            {
                state ^= 1 << (ch - 'a');
                totalCount += stateCount[state];

                for (int i = 0; i < 10; i++)
                {
                    totalCount += stateCount[state ^ (1 << i)];
                }

                stateCount[state]++;
            }

            return totalCount;
        }

        private static void SetBordIsland(char[][] board, int row, int col)
        {
            board[row][col] = '.';

            //  move to left
            if (board[row][Math.Max((col - 1), 0)] == 'O')
            {
                SetBordIsland(board, row, Math.Max((col - 1), 0));
            }

            //  move to top
            if (board[Math.Max((row - 1), 0)][col] == 'O')
            {
                SetBordIsland(board, Math.Max((row - 1), 0), col);
            }

            //  move to right
            if (board[row][Math.Min((col + 1), board[0].Length - 1)] == 'O')
            {
                SetBordIsland(board, row, Math.Min((col + 1), board[0].Length - 1));
            }

            //  move to bottom
            if (board[Math.Min((row + 1), board.Length - 1)][col] == 'O')
            {
                SetBordIsland(board, Math.Min((row + 1), board.Length - 1), col);
            }
        }

        public static void Solve(char[][] board)
        {
            //  130.Surrounded Regions

            //  assume that count of row and col are equal
            for (int j = 0; j < board[0].Length; j++)
            {
                if (board[0][j] == 'O')
                {
                    SetBordIsland(board, 0, j);
                }

                if (board[board.Length - 1][j] == 'O')
                {
                    SetBordIsland(board, board.Length - 1, j);
                }
            }

            for (int i = 0; i < board.Length; i++)
            {
                if (board[i][0] == 'O')
                {
                    SetBordIsland(board, i, 0);
                }
                if (board[i][board[0].Length - 1] == 'O')
                {
                    SetBordIsland(board, i, board[0].Length - 1);
                }
            }

            for (int i = 0; i < board.Length; i++)
            {
                for (int j = 0; j < board[0].Length; j++)
                {
                    if (board[i][j] == 'O')
                    {
                        board[i][j] = 'X';
                    }
                    if (board[i][j] == '.')
                    {
                        board[i][j] = 'O';
                    }
                }
            }
        }

        private static void GetCountIsland(char[][] grid, int row, int col)
        {
            grid[row][col] = '0';

            //  move to left
            if (grid[row][Math.Max((col - 1), 0)] == '1')
            {
                GetCountIsland(grid, row, Math.Max((col - 1), 0));
            }

            //  move to top
            if (grid[Math.Max((row - 1), 0)][col] == '1')
            {
                GetCountIsland(grid, Math.Max((row - 1), 0), col);
            }

            //  move to right
            if (grid[row][Math.Min((col + 1), grid[0].Length - 1)] == '1')
            {
                GetCountIsland(grid, row, Math.Min((col + 1), grid[0].Length - 1));
            }

            //  move to bottom
            if (grid[Math.Min((row + 1), grid.Length - 1)][col] == '1')
            {
                GetCountIsland(grid, Math.Min((row + 1), grid.Length - 1), col);
            }
        }

        public static int NumIslands(char[][] grid)
        {
            //  200. Number of Islands
            int islands = 0;
            for (int i = 0; i < grid.Length; i++)
            {
                for (int j = 0; j < grid[0].Length; j++)
                {
                    if (grid[i][j] == '0')
                    {
                        continue;
                    }

                    islands++;
                    GetCountIsland(grid, i, j);
                }
            }

            return islands;
        }

        public static int Rob(int[] nums)
        {
            //  198.House Robber
            if (nums.Length == 1)
            {
                return nums[0];
            }

            if (nums.Length == 2)
            {
                return Math.Max(nums[0], nums[1]);
            }

            int n = nums.Length;
            int[] dp = new int[n];
            int max = 0;
            dp[^1] = nums[^1];
            max = Math.Max(max, dp[^1]);
            dp[^2] = nums[^2];
            max = Math.Max(max, dp[^2]);

            for (int i = nums.Length - 3; i >= 0; i--)
            {
                dp[i] = nums[i] + (Math.Max(dp[i + 2], dp[Math.Min(i + 3, n - 1)]));
                max = Math.Max(max, dp[i]);
            }

            return max;
        }

        public static TreeNode SortedArrayToBST(int[] nums)
        {
            //  108. Convert Sorted Array to Binary Search Tree

            if (nums.Length == 0)
            {
                return null;
            }

            if (nums.Length == 1)
            {
                return new TreeNode(nums[0]);
            }

            int rootIndex = nums.Length / 2;
            TreeNode node = new(nums[rootIndex]);

            TreeNode leftNode = SortedArrayToBST(nums[..rootIndex]);
            TreeNode rightNode = SortedArrayToBST(nums[(rootIndex + 1)..]);

            node._left = leftNode;
            node._right = rightNode;

            return node;
        }

        public static bool IsSubsequence(string s, string t)
        {
            //  392. Is Subsequence

            if (string.IsNullOrEmpty(s))
            {
                return true;
            }

            int start = 0;
            bool found = false;
            int rest = s.Length;
            foreach (char ch in s)
            {
                while (start < t.Length)
                {
                    if (ch == t[start])
                    {
                        found = true;
                        start++;
                        break;
                    }
                    start++;
                }
                rest--;
                if (rest > 0)
                {
                    found = false;
                }
            }
            return found;
        }

        private static void FindPath(int[][] grid, bool[][] vis, int row, int col, int res, ref int sum)
        {
            if (row == grid.Length)
            {
                return;
            }

            for (int j = 0; j < grid[0].Length; j++)
            {
                if (vis[row][j])
                {
                    continue;
                }

                if (row == grid.Length - 1)
                {
                    sum = Math.Min(res + grid[row][j], sum);
                }

                res += grid[row][j];
                vis[(row + 1) < grid.Length ? (row + 1) : row][j] = true;
                FindPath(grid, vis, row + 1, j, res, ref sum);
                vis[(row + 1) < grid.Length ? (row + 1) : row][j] = false;
                res -= grid[row][j];
            }
        }

        public static int MinFallingPathSum(int[][] grid)
        {
            //  1289. Minimum Falling Path Sum II

            bool[][] vis = new bool[grid.Length][];

            for (int i = 0; i < grid.Length; i++)
            {
                vis[i] = new bool[grid[0].Length];
            }

            int res = int.MaxValue;
            int sum = int.MaxValue;
            for (int j = 0; j < grid[0].Length; j++)
            {
                if (grid.Length > 1)
                {
                    vis[1][j] = true;
                    FindPath(grid, vis, 1, j, grid[0][j], ref res);
                    sum = Math.Min(res, sum);
                    vis[1][j] = false;
                }
                else
                {
                    sum = Math.Min(grid[0][j], sum);
                }
            }
            return sum;
        }

        public static int MinFallingPathSum2(int[][] grid)
        {
            int[][] dp = new int[grid.Length][];
            dp[0] = new int[grid[0].Length];
            for (int i = 0; i < grid.Length; i++)
            {
                dp[0][i] = grid[0][i];
            }

            int elm;
            for (int i = 1; i < grid.Length; i++)
            {
                dp[i] = new int[grid.Length];
                for (int j = 0; j < grid[0].Length; j++)
                {
                    elm = int.MaxValue;
                    for (int k = 0; k < grid.Length; k++)
                    {
                        if (k != j && dp[i - 1][k] < elm)
                        {
                            elm = dp[i - 1][k];
                        }
                    }
                    dp[i][j] = elm + grid[i][j];
                }
            }

            int sum = int.MaxValue;
            for (int k = 0; k < grid.Length; k++)
            {
                sum = Math.Min(dp[grid.Length - 1][k], sum);
            }
            return sum;
        }

        private static int GetMin(int[] nums, int notUseIndex)
        {
            var min = int.MaxValue;
            for (var index = 0; index < nums.Length; index++)
            {
                if (index == notUseIndex) continue;
                min = Math.Min(min, nums[index]);
            }
            return min;
        }

        public static int LongestConsecutive(int[] nums)
        {
            //  128. Longest Consecutive Sequence

            HashSet<int> sets = new(nums);

            int res = 0;
            foreach (int num in sets)
            {
                if (sets.Contains(num - 1))
                {
                    continue;
                }

                int inNum = num;
                int inRes = 0;
                while (sets.Contains(++inNum))
                {
                    inRes++;
                }
                res = Math.Max(res, inRes + 1);
            }
            return res;
        }

        private void Reverse(int[] nums, int left, int right)
        {
            int tmp = 0;
            while (left < right)
            {
                tmp = nums[left];
                nums[left] = nums[right];
                nums[right] = tmp;

                left++;
                right--;
            }
        }

        public void Rotate(int[] nums, int k)
        {
            //  189.Rotate Array

            if (nums.Length == 1)
            {
                return;
            }
            k %= nums.Length;

            Reverse(nums, 0, nums.Length - 1);
            Reverse(nums, 0, k - 1);
            Reverse(nums, k, nums.Length - 1);
        }

        public static int RomanToInt(string s)
        {
            //  13. Roman to Integer
            Dictionary<string, int> dic = new()
            {
                { "I", 1 }, { "V", 5}, { "X", 10},
                { "IV", 4}, {"IX", 9},
                { "L", 50}, {"C", 100}, {"D", 500},
                { "XL", 40}, {"XC", 90},
                { "M", 1000},
                { "CD", 400}, {"CM", 900}
            };

            int i = 0;
            int res = 0;
            int left = 0;
            while(i < s.Length)
            {
                string ss = s[left..Math.Min(i + 2, s.Length)];
                if (dic.TryGetValue(ss, out int val))
                {
                    left += 2;
                    i += 2;
                }
                else
                {
                    ss = s[left..Math.Min(i + 1, s.Length)];
                    dic.TryGetValue(ss, out val);

                    left++;
                    i++;
                }
                res += val;
            }

            return res;
        }

        private static int Longest(string s, int k)
        {
            int index = 0;
            int lastCharIndex = -1;
            int length = 1;
            while (index < s.Length)
            {
                if (lastCharIndex == -1)
                {
                    lastCharIndex = 'z' - (s[index]);
                }
                else
                {
                    if (Math.Abs('z' - (s[index]) - lastCharIndex) <= k)
                    {
                        lastCharIndex = 'z' - (s[index]);
                        length++;
                    }
                }

                index++;
            }
            return length;
        }

        public static int LongestIdealString(string s, int k)
        {
            //  2370.Longest Ideal Subsequence
            int[] dp = new int[26];
            int res = 0;
            foreach (var ch in s)
            {
                int index = ch - 'a';

                int appearance = 0;
                for (int i = Math.Min(0, index - k); i < Math.Max(26, index + k + 1); i++)
                {
                    appearance = Math.Max(appearance, dp[i]);
                }

                dp[index] = appearance + 1;
                res = Math.Max(res, dp[index]);
            }

            return res;
        }

        public static ListNodeN ReverseBetween(ListNodeN head, int left, int right)
        {
            //  92. Reverse Linked List II
            if (head == null || left == right)
            {
                return head;
            }

            ListNodeN dummy = new();
            dummy.next = head;
            ListNodeN prev = dummy;

            for (int i = 0; i < left - 1; ++i)
            {
                prev = prev.next;
            }

            ListNodeN current = prev.next;

            for (int i = 0; i < right - left; ++i)
            {
                ListNodeN nextNode = current.next;
                current.next = nextNode.next;
                nextNode.next = prev.next;
                prev.next = nextNode;
            }

            return dummy.next;
        }

        public static int Tribonacci(int n)
        {
            //  1137.N - th Tribonacci Number
            int[] start = [0, 0, 1, 1, 2];
            if (n <= 3)
            {
                return start[n + 1];
            }

            int[] res = new int[n + 2];
            start.CopyTo(res, 0);

            for (int i = start.Length ; i <= n + 1; i++)
            {
                res[i] = res[i - 1] + res[i - 2] + res[i - 3];
            }

            return res[n + 1];
        }

        public static ListNodeN RotateRight(ListNodeN head, int k)
        {
            if (head == null || k == 0)
            {
                return head;
            }

            ListNodeN start = head;

            int size = 1; 
            while (head.next != null)
            {
                head = head.next;
                size++;
            }
            ListNodeN end = head;

            end.next = start;
            k = size - (k % size);

            while (k > 0)
            {
                start = start.next;
                end = end.next;
                k--;
            }
            end.next = null;
            return start;
        }

        public static void Merge2(int[] nums1, int m, int[] nums2, int n)
        {
            int i = m - 1;
            int j = n - 1;
            int k = m + n - 1;

            while (j >= 0)
            {
                if (i >= 0 && nums1[i] > nums2[j])
                {
                    nums1[k] = nums1[i];
                    i--;
                }
                else
                {
                    nums1[k] = nums2[j];
                    j--;
                }
                k--;
            }
        }

        public class ListNodeN
        {
            public int val;
            public ListNodeN next;
            public ListNodeN(int val = 0, ListNodeN next = null)
            {
                this.val = val;
                this.next = next;
            }
        }

        public static ListNodeN MergeTwoLists(ListNodeN list1, ListNodeN list2)
        {
            ListNodeN current = new();
            ListNodeN linkToCurrent = current;
            while (list1 != null && list2 != null)
            {
                if (list1.val <= list2.val)
                {
                    current.next = list1;
                    list1 = list1.next;
                }
                else
                {
                    current.next = list2;
                    list2 = list2.next;
                }
                current = current.next;
            }
            current.next = list1 ?? list2;

            return linkToCurrent.next;
        }

        public static ListNodeN SortList(ListNodeN head)
        {
            //  148.Sort List
            
            if (head == null || head.next == null)
            {
                return head;
            }

            ListNodeN slow = head;
            ListNodeN fast = head.next;

            while (fast != null && fast.next != null)
            {
                slow = slow.next;
                fast = fast.next.next;
            }

            ListNodeN t = slow.next;
            slow.next = null;

            ListNodeN leftL = SortList(head);
            ListNodeN rightL = SortList(t);

            ListNodeN d = new ListNodeN();
            ListNodeN current = d;
            while (leftL != null && rightL != null)
            {
                if (leftL.val <= rightL.val)
                {
                    current.next = leftL;
                    leftL = leftL.next;
                }
                else
                {
                    current.next = rightL;
                    rightL = rightL.next; 
                }
                current = current.next;
            }

            current.next = leftL ?? rightL;

            return d.next;
        }

        public static int findKthLargest(int[] nums, int k)
        {
            //  215. Kth Largest Element in an Array
            PriorityQueue<int, int> queue = new PriorityQueue<int, int>();
            foreach (int item in nums)
            {
                queue.Enqueue(item, item);
                if (queue.Count > k)
                {
                    queue.Dequeue();
                }
            }

            return queue.Dequeue();
        }

        public static string ReverseWords(string s)
        {
            //  151. Reverse Words in a String
            Queue<string> queue = new();

            int left = s.Length - 1;
            int leftleft = left;
            while (leftleft >= 0) 
            {
                if (s[leftleft] == ' ')
                {
                    if (leftleft < left)
                    {
                        queue.Enqueue(s.Substring(leftleft + 1, left - leftleft));
                    }

                    leftleft--;
                    left = leftleft;
                    continue;
                }
                leftleft--;
            }

            if (leftleft < left)
            {
                queue.Enqueue(s.Substring(leftleft + 1, left - leftleft));
            }

            return string.Join(' ', queue.ToArray());
        }

        public static int LengthOfLastWord(string s)
        {
            int r = s.Length - 1;
            int count = 0;
            while (r >= 0)
            {
                if (s[r] == ' ')
                {
                    if (count == 0)
                    {
                        r--;
                        continue;
                    }
                    else
                    {
                        break;
                    }
                }
                count++;
                r--;
            }

            return count;
        }

        private static bool Move(int toIndex, int[] gas, int[] cost)
        {
            int gases = 0;
            int consts = 0;

            int index;
            int step = 0;
            for (int i = 0; i < gas.Length; i++)
            {
                index = i + toIndex >= gas.Length ? (i + toIndex) - gas.Length : i + toIndex;
                gases += (gas[index] - consts);
                consts = cost[index];

                if (gases < consts || (gases == 0 && consts == 0))
                {
                    return false;
                }

                if (step == gas.Length - 1 && gases >= consts)
                {
                    return true;
                }
                step++;
            }
            return true;
        }

        public static int CanCompleteCircuit(int[] gas, int[] cost)
        {
            if (gas.Length == 1)
            {
                return gas[0] >= cost[0] ? 0 : -1;
            }

            bool exist;
            for (int i = 0; i < gas.Length; i++)
            {
                if (gas[i] <= cost[i])
                {
                    continue;
                }

                exist = Move(i, gas, cost);
                if (exist)
                {
                    return i;
                }
            }
            return -1;
        }

        private static void BackTrc(int opened, int closed, List<string> list, int n, string str)
        {
            if (str.Length == n * 2)
            {
                if (opened == closed)
                {
                    list.Add(str);
                }
                return;
            }

            if (opened < n || opened == 0)
            {
                BackTrc(opened + 1, closed, list, n, str + '(');
            }

            if (closed < opened)
            {
                BackTrc(opened, closed + 1, list, n, str + ')');
            }
        }

        public static IList<string> GenerateParenthesis(int n)
        {
            List<string> list = new();
            BackTrc(0, 0, list, n, "");

            return list;
        }

        public static int MaxProfit(int[] prices)
        {
            int min = prices[0];
            int profit = 0;

            for (int i = 1; i < prices.Length; i++)
            {
                if (prices[i] < min)
                {
                    min = prices[i];
                }
                else
                {
                    if (prices[i] - min > profit)
                    {
                        profit = prices[i] - min;
                    }
                }
            }

            return profit;
        }

        public static int MajorityElement(int[] nums)
        {
            int count = 0;
            int search = 0;
            foreach(int num in nums)
            {
                if (count == 0)
                {
                    search = num;
                    count++;
                }
                else
                {
                    count += search == num ? 1 : -1;
                }
            }        
            return search;
        }

        public static int RemoveDuplicates2(int[] nums)
        {
            //  26.Remove Duplicates from Sorted Array
            int left = 0;
            foreach (int num in nums)
            {
                if (left == 0 || num != nums[left - 1])
                {
                    nums[left] = num;
                    left++;
                }
            }
            return left;
        }

        public static int RemoveElement(int[] nums, int val)
        {
            //  27.Remove Element

            int index = 0;
            int right = nums.Length - 1;
            while (index < right)
            {
                if (nums[index] == val)
                {
                    while (right > index && nums[right] == val)
                    {
                        right--;
                    }
                    if (right < 0 || nums[right] == val)
                    {
                        break;
                    }

                    int tmp = nums[index]; 
                    nums[index] = nums[right];
                    nums[right] = tmp;
                }

                index++;
            }
            return index + (nums.Length > 0 && nums[^1] != val ? 1 : 0);
        }

        public static int SingleNumber2(int[] nums)
        {
            //  137.Single Number II
            //  array for holding bit numbers for each number
            int[] bits = new int[32];

            foreach (var item in nums)
            {
                for (int i = 31; i >= 0; i--)
                {
                    bits[i] += (item >> i) & 1;
                }
            }

            int res = 0;
            for (int i = 31; i >= 0; i--)
            {
                res <<= 1;
                bits[i] %= 3;

                res |= bits[i];
            }

            return res;
        }

        public static int SingleNumber(int[] nums)
        {
            //  136.Single Number

            int res = 0;
            foreach (var num in nums)
            {
                res ^= num;
            }
            return res;
        }

        public static int HammingWeight(int n)
        {
            //  191.Number of 1 Bits
            int res = 0;
            while(n > 0)
            {
                res += (n & 1);
                n >>= 1;
            }

            return res;
        }

        public static uint reverseBits(uint n)
        {
            //  190.Reverse Bits

            uint result = 0;

            for (int i = 0; i < 32 && n > 0; ++i)
            {
                result |= (n & 1) << (31 - i);

                n >>= 1;
            }

            return result >> 0;
        }

        public static string AddBinary(string a, string b)
        {
            //  67.Add Binary

            Stack<char> stack = new();

            a = a.PadLeft(b.Length, '0');
            b = b.PadLeft(a.Length, '0');

            byte inMind = 0;
            byte left;
            byte right;
            byte res;

            for (int i = a.Length - 1; i >= 0; i--)
            {
                left = (byte)(a[i] - '0');
                right = (byte)(b[i] - '0');

                if ((left & right) == 1)
                {
                    if (inMind > 0)
                    {
                        stack.Push('1');
                    }
                    else
                    {
                        stack.Push('0');
                        inMind++;
                    }
                }
                else
                if ((left | right) == 1)
                {
                    if (inMind > 0) 
                    {
                        stack.Push('0');
                    }
                    else
                    {
                        stack.Push('1');
                    }
                }
                else
                {
                    if (inMind > 0)
                    {
                        stack.Push('1');
                        inMind--;
                    }
                    else
                    {
                        stack.Push('0');
                    }
                }
            }
                
            while (inMind > 0)
            {
                stack.Push('1');
                inMind--;
            }

            return string.Join("", stack.ToArray());
        }

        private static void SearchDeeply(char[][] board, string word, bool[][] vis,
            ref int searchIndex, int row, int col, ref bool res)
        {
            if (board[row][col] != word[searchIndex - 1] || res)
            {
                return;
            }

            if (searchIndex == word.Length)
            {
                res = true;
                return;
            }

            while (row < board.Length && col < board[0].Length)
            {
                //  go to the left
                if ((col - 1) >= 0 && !vis[row][col - 1])
                {
                    vis[row][col - 1] = true;
                    searchIndex++;
                    SearchDeeply(board, word, vis, ref searchIndex, row, col - 1, ref res);
                    searchIndex--;
                    vis[row][col - 1] = false;
                }

                //  go to the top
                if ((row - 1) >= 0 && !vis[row - 1][col])
                {
                    vis[row - 1][col] = true;
                    searchIndex++;
                    SearchDeeply(board, word, vis, ref searchIndex, row - 1, col, ref res);
                    searchIndex--;
                    vis[row - 1][col] = false;
                }

                //  go to the right
                if ((col + 1) < board[0].Length && !vis[row][col + 1])
                {
                    vis[row][col + 1] = true;
                    searchIndex++;
                    SearchDeeply(board, word, vis, ref searchIndex, row, col + 1, ref res);
                    searchIndex--;
                    vis[row][col + 1] = false;
                }

                //  go to the bottom
                if ((row + 1) < board.Length && !vis[row + 1][col])
                {
                    vis[row + 1][col] = true;
                    searchIndex++;
                    SearchDeeply(board, word, vis, ref searchIndex, row + 1, col, ref res);
                    searchIndex--;
                    vis[row + 1][col] = false;
                }

                return;
            }
        }

        public static bool Exist(char[][] board, string word)
        {
            //  79.Word Search
            int searchIndex = 0;
            bool[][] vis = new bool[board.Length][];
            for (int i = 0; i < board.Length; i++)
            {
                vis[i] = new bool[board[0].Length];
            }

            bool res = false;
            for (int i = 0; i < board.Length; i++)
            {
                for (int j = 0; j < board[0].Length; j++)
                {
                    vis[i][j] = true;
                    searchIndex++;
                    SearchDeeply(board, word, vis, ref searchIndex, i, j, ref res);
                    searchIndex--;
                    vis[i][j] = false;
                    if (res)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private static bool Contains(int[] visited, List<int[]> tmpVisit)
        {
            for (int i = 0; i < tmpVisit.Count; i++)
            {
                bool b = true;
                for (int j = 0; j < tmpVisit[i].Length; j++)
                {
                    b = b & visited[j] == tmpVisit[i][j];
                }
                if (b)
                {
                    return b;
                }
            }
            return false;
        }

        private static void Dfs(int[] candidates, int target, int sum, IList<int> tmp,
            List<IList<int>> result, int[] visited, List<int[]> tmpVisit)
        {
            if (sum >= target)
            {
                if (sum == target && !Contains(visited, tmpVisit))
                {
                    result.Add(new List<int>(tmp));
                    int[] t = new int[candidates.Length];
                    visited.CopyTo(t, 0);
                    tmpVisit.Add(t);
                }
                return;
            }

            for (int i = 0; i < candidates.Length; i++)
            {
                visited[i]++;
                tmp.Add(candidates[i]);
                Dfs(candidates, target, sum + candidates[i], tmp, result, visited, tmpVisit);
                tmp.RemoveAt(tmp.Count - 1);
                visited[i]--;
            }
        }

        public static IList<IList<int>> CombinationSum(int[] candidates, int target)
        {
            //  39. Combination Sum

            List<IList<int>> res = [];
            int[] visited = new int[candidates.Length];
            List<int[]> tmpVisit = [];
            Dfs(candidates, target, 0, [], res, visited, tmpVisit);
            return res;
        }

        private static void SearchDeep(int[] nums, int index, bool[] visited, List<int> permut, List<IList<int>> res)
        {
            if (index == nums.Length)
            {
                res.Add(new List<int>(permut));
                return;
            }

            for (int i = 0; i < nums.Length; ++i)
            {
                if (!visited[i])
                {
                    visited[i] = true;
                    permut.Add(nums[i]);
                    SearchDeep(nums, index + 1, visited, permut, res);

                    permut.RemoveAt(permut.Count - 1);
                    visited[i] = false;
                }
            }
        }

        public static IList<IList<int>> Permute(int[] nums)
        {
            //  46.Permutations

            List<IList<int>> res = [[]];
            List<int> perm = new List<int>();

            SearchDeep(nums, 0, new bool[nums.Length], perm, res);
            return res;
        }

        public static void SearchArray(int[] res, int index, int lenght, IList<int> tmp, List<IList<int>> arr)
        {
            if (index > res.Length - 1 || tmp.Count == lenght)
            {
                if (tmp.Count == lenght)
                {
                    arr.Add(tmp);
                }
                return;
            }

            for (int i = index; i < res.Length; i++)
            {
                IList<int> tmp2 = new List<int>(tmp) { res[i] };
                SearchArray(res, i + 1, lenght, tmp2, arr);
            }

            /*
            List<int> tmp = [res[index]];
            for (int i = index + 1; i < res.Length; i++) 
            {
                if (tmp.Count >= lenght)
                {
                    arr.Add(tmp);
                    tmp = [res[index]];
                }
                tmp.Add(res[i]);
            }
            if (tmp.Count == lenght)
            {
                arr.Add(tmp);
            }
            */

            //SearchArray(res, index + 1, lenght, arr);
        }

        public static IList<IList<int>> Combine(int n, int k)
        {
            if (k == 1)
            {
                List<IList<int>> l = [];
                for (int i = 0; i < n; i++)
                {
                    l.Add([i + 1]);
                }
                return l;
            }

            int[] res = new int[n];
            for (int i = 0; i < n; i++)
            {
                res[i] = i + 1;
            }

            List<IList<int>> arr = new List<IList<int>>();
            for (int i = 0; i <= res.Length - k; i++)
            {
                SearchArray(res, i + 1, k, [res[i]], arr);
            }


            return arr;
        }

        private static Dictionary<char, string> Dic = new Dictionary<char, string>()
        {
            { '1', "" },
            { '2', "abc" },
            { '3', "def" },
            { '4', "ghi" },
            { '5', "jkl" },
            { '6', "mno" },
            { '7', "pqrs" },
            { '8', "tuv" },
            { '9', "wxyz" }
        };

        private static void SearchCh(string digits, int index, string str, List<string> list)
        {
            if (index >= digits.Length)
            {
                if (!string.IsNullOrEmpty(str))
                {
                    list.Add(str);
                }

                return;
            }

            string data = Dic[digits[index]];

            foreach (char ch in data)
            {
                SearchCh(digits, index + 1, str + ch, list);
            }
        }

        public static IList<string> LetterCombinations(string digits)
        {
            //  17. Letter Combinations of a Phone Number
            List<string> list = new List<string>();

            {
                SearchCh(digits, 0, "", list);
            }

            return list;
        }

        public static void Merge(int[] nums1, int m, int[] nums2, int n)
        {
            int i = m - 1;
            int j = n - 1;
            int k = m + n - 1;

            while (j >= 0)
            {
                if (i >= 0 && nums1[i] > nums2[j])
                {
                    nums1[k] = nums1[i];
                    i--;
                }
                else
                {
                    nums1[k] = nums2[j];
                    j--;
                }
                k--;
            }

            /*
            int two = 0;
            int i = 0;
            while (i < m + n)
            { 
                if (two < n && nums2[two] <= nums1[i])
                {
                    for (int j = nums1.Length - 1; j > i; j--) 
                    {
                        nums1[j] = nums1[j - 1];
                    }
                    nums1[i] = nums2[two++];
                }
                else
                {
                    if (i >= m && two < n && nums1[i] == 0)
                    {
                        nums1[i] = nums2[two++];
                    }
                }
                i++;
            }*/
        }

        private static (int, int) ToArrayIndex(int pos, int rows, int columns)
        {
            int row = pos / columns;
            int column = pos - (row * columns);
            return (row, column);
        }

        public static bool SearchMatrix(int[][] matrix, int target)
        {
            //  74.Search a 2D Matrix

            int rows = matrix.Length;
            int columns = 0;
            if (rows > 0)
            {
                columns = matrix[0].Length;
            }
            int nums = rows * columns;

            int left = 0;
            int right = nums - 1;

            int mid = 0;
            int r;
            int c;
            while (left < right)
            {
                mid = (left + right) >> 1;
                (r, c) = ToArrayIndex(mid, rows, columns);

                if (matrix[r][c] >= target)
                {
                    right = mid;
                }
                else
                {
                    left = mid + 1;
                }
            }

            (r, c) = ToArrayIndex(left, rows, columns);

            return matrix[r][c] == target;
        }

        public static int[] SearchRange(int[] nums, int target)
        {
            //  34.Find First and Last Position of Element in Sorted Array
            if (nums.Length == 1)
            {
                if (nums[0] == target)
                {
                    return [0, 0];
                }
            }

            int left = 0;
            int right = nums.Length - 1;

            int mid;
            while (left < right)
            {
                mid = (left + right) >> 1;

                if (nums[mid] == target)
                {
                    right = mid;
                }
                else
                if (nums[mid] < target)
                {
                    left = mid + 1;
                }
                else
                {
                    right = mid;
                }
            }

            if (nums[left] == target)
            {
                int ext = 1;
                while (left + ext < nums.Length && nums[left + ext] == target)
                {
                    ext++;
                }
                return [left, left + ext - 1];
            }

            return [-1, -1];
        }

        public static int Search(int[] nums, int target)
        {
            //  33. Search in Rotated Sorted Array

            int left = 0;
            int right = nums.Length - 1;

            int middle;
            while (left < right)
            {
                middle = (left + right) >> 1;  // / 2

                if (nums[0] <= nums[middle])
                {
                    if (nums[0] <= target && target <= nums[middle])
                    {
                        right = middle;
                    }
                    else
                    {
                        left = middle + 1;
                    }
                }
                else
                if (nums[middle] < target && target <= nums[nums.Length - 1])
                {
                    left = middle + 1;
                }
                else
                {
                    right = middle;
                }

            }
            return nums[left] == target ? left : -1;
        }

        public static int FindPeakElement(int[] nums)
        {
            if (nums.Length < 2)
            {
                return 0;
            }

            int split = nums.Length / 2;
            int leftCore = split;
            int rightCore = split;

            bool res;
            while (leftCore >= 0 || rightCore < nums.Length)
            {
                res = leftCore >= nums.Length - 1 || nums[leftCore] > nums[leftCore + 1];
                if (res)
                {
                    if (leftCore == 0 || nums[leftCore - 1] < nums[leftCore])
                    {
                        return leftCore;
                    }
                }
                leftCore--;

                res = nums[rightCore - 1] < nums[rightCore];
                if (res)
                {
                    if (rightCore >= nums.Length - 1 || nums[rightCore] > nums[rightCore + 1])
                    {
                        return rightCore;
                    }
                }
                /*
                if (nums[rightCore - 1] < nums[rightCore])
                {
                    if (rightCore < nums.Length - 1)
                    {
                        if (nums[rightCore] > nums[rightCore + 1])
                        {
                            return rightCore;
                        }
                    }
                    else
                    {
                        return rightCore;
                    }
                }*/

                rightCore++;
            }

            return -1;

            /*if (nums.Length < 3)
            {
                return -1;
            }

            if (nums.Length / 3 < 2)
            {
                for (int i = 1; i + 1 <= nums.Length; i++)
                {
                    if (nums[i - 1] < nums[i] && nums[i] > nums[i + 1])
                    {
                        return i;
                    }
                }
            }*/


        }

        public static int SearchInsert(int[] nums, int target)
        {
            //  35. Search Insert Position
            if (nums.Length == 0 || nums[0] > target)
            {
                return 0;
            }

            if (nums[nums.Length - 1] < target)
            {
                return nums.Length;
            }

            int split = nums.Length / 2;

            if (nums[split] == target)
            {
                return split;
            }
            else
            if (target < nums[split])
            {
                return SearchInsert(nums[..split], target);
            }
            else
            {
                return split + SearchInsert(nums[split..], target);
            }
        }

        public static bool IsPalindrome(string str)
        {
            StringBuilder builder = new();
            foreach (char c in str)
            {
                if (char.IsLetterOrDigit(c))
                {
                    builder.Append(char.ToLower(c));
                }
            }
            string s = builder.ToString();
            int left = 0;
            int right = s.Length - 1;
            while (left < right)
            {
                if (s[left] != s[right])
                {
                    return false;
                }
                left++;
                right--;
            }
            return true;
        }

        private static void Check(TreeNode node)
        {
            //  98.Validate Binary Search Tree

            if (node == null)
            {
                return;
            }

            Check(node._left);

            if (_prev != null)
            {
                if (_prev >= node._val)
                {
                    _checked = false;
                }
            }
            _prev = node._val;

            Check(node._right);
        }

        public static string EncodeFile(string[] lines)
        {
            SortedDictionary<int, string> dic = new();

            string[] codeLine;
            foreach (string line in lines)
            {
                codeLine = line.Split(' ');
                dic[Convert.ToInt32(codeLine[0])] = codeLine[1];
            }

            int i = 1;
            int lastSum = 1;
            string str;
            StringBuilder builder = new();

            while (dic.TryGetValue(lastSum, out str))
            {
                builder.Append(lastSum == 1 ? str : $" {str}");

                i++;
                lastSum += i;
            }

            return builder.ToString();
        }

        public class Solution
        {
            private Dictionary<int, int> _dic = new();
            private int[] _preorder;
            private int[] _inorder;

            private TreeNode CreateNodeDfs(int preIndex, int inIndex, int size)
            {
                if (size <= 0)
                {
                    return null;
                }

                int rootVal = _preorder[preIndex];
                _dic.TryGetValue(rootVal, out int indexRoot);
                int leftSize = indexRoot - inIndex;

                TreeNode leftSet = CreateNodeDfs(preIndex + 1, inIndex, leftSize);
                TreeNode rightSet = CreateNodeDfs(preIndex + 1 + leftSize, indexRoot + 1, size - 1 - leftSize);

                return new TreeNode(rootVal, leftSet, rightSet);
            }

            public TreeNode BuildTree(int[] preorder, int[] inorder)
            {
                _preorder = preorder;
                _inorder = inorder;

                for (int i = 0; i < inorder.Length; i++)
                {
                    _dic[inorder[i]] = i;
                }

                return CreateNodeDfs(0, 0, preorder.Length);
            }
        }

        public static bool Recursive(ref int sum)
        {
            if (sum == 10)
            {
                return true;
            }

            sum++;
            bool res = Recursive(ref sum);

            return res;
        }

        public static string SimplifyPath(string path)
        {
            //  71.Simplify Path

            Stack<string> stack = new();
            foreach (var part in path.Split('/').ToArray())
            {
                if (string.IsNullOrEmpty(part) || part == ".")
                {
                    continue;
                }

                List<string> st = new();


                if (part == "..")
                {
                    if (stack.Count > 0)
                    {
                        stack.Pop();
                    }
                    continue;
                }
                stack.Push(part);
            }

            return '/' + string.Join('/', stack.Reverse().ToArray());
        }

        public static int[][] Merge(int[][] intervals)
        {
            //56.Merge Intervals
            Array.Sort(intervals, (left, right) =>
            {
                return left[0].CompareTo(right[0]);
            });

            Stack<(int left, int right)> ranges = new();

            int cleft;
            int cright;
            int lastleft = int.MinValue;
            int lastright = int.MinValue;

            for (int i = 0; i < intervals.Length; i++)
            {
                cleft = intervals[i][0];
                cright = intervals[i][1];

                if (ranges.Count > 0)
                {
                    (lastleft, lastright) = ranges.Peek();
                }

                if (cleft <= lastright)
                {
                    ranges.Pop();
                    ranges.Push((cleft < lastleft ? cleft : lastleft, lastright < cright ? cright : lastright));
                }
                else
                {
                    ranges.Push((cleft, cright));
                }
            }

            int[][] res = new int[ranges.Count][];
            int index = ranges.Count - 1;
            while (ranges.Count > 0)
            {
                (lastleft, lastright) = ranges.Pop();

                res[index] = new int[2];
                res[index][0] = lastleft;
                res[index][1] = lastright;

                index--;
            }
            return res;
        }

        public static bool IsAnagram(string s, string t)
        {
            //242.Valid Anagram

            string left = string.Create(s.Length, s, (c, b) =>
            {
                char[] barr = b.ToCharArray();
                Array.Sort(barr);
                barr.CopyTo(c);
            });

            string right = string.Create(t.Length, t, (c, b) =>
            {
                char[] barr = b.ToCharArray();
                Array.Sort(barr);
                barr.CopyTo(c);
            });

            return left == right;
        }

        public static int[] PlusOne(int[] digits)
        {
            int res;
            bool needNewArray = false;
            for (int i = digits.Length - 1; i >= 0; i--)
            {
                res = digits[i] + 1;

                if (res == 10)
                {
                    digits[i] = 0;
                    needNewArray = true;
                }
                else
                {
                    digits[i] = res;
                    needNewArray = false;
                    break;
                }
            }
            if (needNewArray)
            {
                int[] newArray = new int[digits.Length + 1];
                newArray[0] = 1;
                digits.CopyTo(newArray, 1);
                return newArray;
            }

            return digits;
        }

        public static bool isHappy(int n)
        {
            int res = n.ToString().ToArray().Select(x => (int)Math.Pow(x - '0', 2)).Sum();

            if (res == 1)
            {
                return true;
            }

            if (res == 4)
            {
                return false;
            }

            return isHappy(res);
        }

        public static IList<string> SummaryRanges(int[] nums)
        {
            //  228.Summary Ranges
            List<string> ranges = [];

            if (nums.Length == 0)
            {
                return ranges;
            }

            int left = 0;
            for (int i = 1; i < nums.Length; i++)
            {
                if (Math.Abs(nums[i] - nums[i - 1]) > 1)
                {
                    if (i - left == 1)
                    {
                        ranges.Add(nums[left].ToString());
                        left = i;
                    }
                    else
                    {
                        ranges.Add($"{nums[left]}->{nums[i - 1]}");
                        left = i;
                    }
                }
            }

            if (nums.Length - left == 1)
            {
                ranges.Add(nums[left].ToString());
            }
            else
            {
                ranges.Add($"{nums[left]}->{nums[^1]}");
            }
            return ranges;
        }

        public static void GameOfLife(int[][] board)
        {
            //  289. Game of Life

            //  calculate amount of alive neighbors
            int aliveNeigbors = 0;
            for (int row = 0; row < board.Length; row++)
                for (int col = 0; col < board[row].Length; col++)
                {
                    aliveNeigbors = 0;
                    for (int row_n = Math.Max(row - 1, 0); row_n < Math.Min(row + 2, board.Length); row_n++)
                        for (int col_n = Math.Max(col - 1, 0); col_n < Math.Min(col + 2, board[row].Length); col_n++)
                        {
                            aliveNeigbors += board[row_n][col_n] % 2;
                        }

                    if (aliveNeigbors == 3 || aliveNeigbors == 4)
                    {
                        board[row][col] += 2;
                    }
                }
            for (int row = 0; row < board.Length; row++)
                for (int col = 0; col < board[row].Length; col++)
                {
                    board[row][col] %= 2;
                }
        }

        public static int MinSubArrayLen(int target, int[] nums)
        {
            int left = 0;
            int right = 0;
            int min = int.MaxValue;
            int sum = 0;

            while (right < nums.Length)
            {
                sum += nums[right];
                right++;

                while (sum >= target)
                {
                    min = Math.Min(min, right - left);
                    sum -= nums[left];
                    left++;
                }
            }
            return min == int.MaxValue ? 0 : min;
        }

        public static int[] TwoSum2(int[] numbers, int target)
        {
            //167.Two Sum II -Input Array Is Sorted

            Dictionary<int, List<int>> dic = new Dictionary<int, List<int>>();
            List<int> arr;
            for (int i = 0; i < numbers.Length; i++)
            {
                if (dic.TryGetValue(numbers[i], out arr))
                {
                    arr.Add(i);
                }
                else
                {
                    arr = new List<int>() { i };
                }
                dic[numbers[i]] = arr;
            }

            int subs;
            int leftIndex;
            int rightIndex;
            List<int> pos;

            for (int i = 0; i < numbers.Length; i++)
            {
                leftIndex = i;
                subs = target - numbers[i];

                if (dic.TryGetValue(subs, out pos))
                {
                    for (int j = 0; j < pos.Count; j++)
                    {
                        if (pos[j] > i)
                        {
                            rightIndex = pos[j];
                            return [leftIndex, pos[j]];
                        }
                    }
                }
            }
            return [-1, -1];
        }

        public static int RemoveDuplicates(int[] nums)
        {
            int lastIndex = 0;
            int countRepeat = 0;
            int currentIndex = 0;
            int lastNumber = -1;

            while (currentIndex < nums.Length)
            {
                if (nums[currentIndex] == lastNumber || currentIndex == 0)
                {
                    countRepeat++;
                }
                else
                {
                    countRepeat = 1;
                }

                lastNumber = nums[currentIndex];
                nums[lastIndex] = lastNumber;

                if (countRepeat <= 2)
                {
                    lastIndex++;
                }

                currentIndex++;
            }

            return lastIndex;
        }

        public static int[] TwoSum(int[] nums, int target)
        {
            Dictionary<int, List<int>> dic = new Dictionary<int, List<int>>();
            List<int> arr;
            for (int i = 0; i < nums.Length; i++)
            {
                if (dic.TryGetValue(nums[i], out arr))
                {
                    arr.Add(i);
                }
                else
                {
                    arr = new List<int>() { i };
                }
                dic[nums[i]] = arr;
            }

            int indexOne = 0;
            int indexOneValue = 0;
            int indexTwo = 0;
            List<int> indexs;
            bool found = false;
            for (int i = 0; i < nums.Length; i++)
            {
                indexOne = i;
                indexOneValue = nums[i];
                if (dic.TryGetValue(target - indexOneValue, out indexs))
                {
                    for (int j = 0; j < indexs.Count; j++)
                    {
                        if (indexs[j] > i)
                        {
                            indexTwo = indexs[j];
                            found = true;
                            break;
                        }
                    }
                    if (found)
                    {
                        break;
                    }
                }
            }
            return new int[2] { indexOne, indexTwo };
        }

    }
}
